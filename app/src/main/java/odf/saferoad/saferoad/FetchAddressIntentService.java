package odf.saferoad.saferoad;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.ResultReceiver;
import android.util.Log;
import android.widget.Toast;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.TimeZone;

import static odf.saferoad.saferoad.HttpClientService.GET;
import static odf.saferoad.saferoad.HttpClientService.POST;

public class FetchAddressIntentService extends Service {

    public static final int CHECK_CONDITIONS_EVERY_X_SECONDS = 15;
    private ServiceHandler mServiceHandler;
    private final String DEBUG_TAG = "UpdateLocation::Service";
    private ResultReceiver resultReceiver;
    private Intent startedIntent;
    private Message message;
    private int serviceStartId;

    public static class NotificationTracker implements Serializable {
        DateTime lastWarning;

        public boolean shouldNotify() {
            if (lastWarning == null) {
                return true;
            }
            DateTime ny = lastWarning;
            DateTime la = DateTime.now();
            Duration duration = new Interval(ny, la).toDuration();
            if (duration.getStandardSeconds() < 120) {
                return false;
            }
            return true;
        }

        public void updateLastNotificationSound() {
            lastWarning = DateTime.now();
        }
    }

    NotificationTracker notificationTracker = new NotificationTracker();

    // Handler that receives messages from the thread
    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {

            MyLocation.LocationResult locationResult = new MyLocation.LocationResult() {
                @Override
                public void gotLocation(Location location) {
                    double longitude = location.getLongitude();
                    double latitude = location.getLatitude();
                    Log.i("location", "main activity longitude:" + longitude);
                    Log.i("location", "main activity latitude:" + latitude);
                    new GetBadZonesRequest(location).execute();
                    mServiceHandler.post(new MakeToast(trackLocation(location)));
                }
            };
            MyLocation myLocation = new MyLocation();
            myLocation.getLocation(FetchAddressIntentService.this, locationResult);
        }
    }

    @Override
    public void onCreate() {
        // Start up the thread running the service.  Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block.  We also make it
        // background priority so CPU-intensive work will not disrupt our UI.
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                android.os.Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();
        Looper mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);

        Log.d(DEBUG_TAG, ">>>onCreate()");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//      Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();

        // For each start request, send a message to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
        resultReceiver = intent.getParcelableExtra("receiver");
        notificationTracker = (NotificationTracker) intent.getSerializableExtra("notificationTracker");
        if (notificationTracker == null) {
            notificationTracker = new NotificationTracker();
        }

        startedIntent = intent;
        message = mServiceHandler.obtainMessage();
        message.arg1 = startId;
        serviceStartId = startId;
        mServiceHandler.sendMessage(message);
        Log.d(DEBUG_TAG, ">>>onStartCommand()");
        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding, so return null
        return null;
    }

    @Override
    public void onDestroy() {
        // I want to restart this service again in one hour
        AlarmManager alarm = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent intent = startedIntent;
        intent.putExtra("notificationTracker", notificationTracker);
        alarm.set(
                AlarmManager.RTC_WAKEUP,
                System.currentTimeMillis() + (1000 * CHECK_CONDITIONS_EVERY_X_SECONDS),
                PendingIntent.getService(this, 0, intent, 0)
        );
    }

    //obtain current location, insert into database and make toast notification on screen
    private String trackLocation(Location location) {
        double longitude;
        double latitude;
        String time;
        String result = "Location currently unavailable.";

        longitude = location.getLongitude();
        latitude = location.getLatitude();
        time = parseTime(location.getTime());
        Log.i("location", "longitude:" + longitude);
        Log.i("location", "latitude:" + latitude);
        Log.i("location", "time:" + time);

        result = "longitude:" + longitude + "latitude:" + latitude;

        return result;
    }

    private String parseTime(long t) {
        DateFormat df = DateFormat.getTimeInstance(DateFormat.MEDIUM);
        df.setTimeZone(TimeZone.getTimeZone("GMT-4"));
        String gmtTime = df.format(t);
        return gmtTime;
    }

    private class MakeToast implements Runnable {
        String txt;

        public MakeToast(String text) {
            txt = text;
        }

        public void run() {
            Toast.makeText(getApplicationContext(), txt, Toast.LENGTH_SHORT).show();
        }
    }

    private class GetBadZonesRequest extends AsyncTask<String, Void, String> {

        private final Location location;

        private GetBadZonesRequest(Location location) {
            this.location = location;
        }

        @Override
        protected String doInBackground(String... any) {
            LocationPoint point = new LocationPoint(location);

            LocationPoint lksPoint = point.toLKS();
            String latitude = "" + lksPoint.getLatitude();
            String longitude = "" + lksPoint.getLongitude();

            latitude = "524567.2466344929";
            longitude = "6101479.764626195";

            String url =
                    "http://lakis.lakd.lt/ArcGIS/rest/services" +
                            "/lakd/avaringi_ruozai/MapServer/identify?f=json" +
                            "&geometry=" +
                            "%7B%22x%22%3A" + latitude + "%2C%22y%22%3A" + longitude + "%2C%22spatialReference%22%3A%7B%22wkid%22%3A3346%7D%7D" +
                            "&tolerance=15" +
                            "&returnGeometry=true" +
                            "&mapExtent=%7B%22xmin%22%3A172353.20887308405%2C%22ymin%22%3A5946539.454745575%2C%22xmax%22%3A857731.2462958256%2C%22ymax%22%3A6259383.4137668265%2C%22spatialReference%22%3A%7B%22wkid%22%3A3346%7D%7D" +
                            "&imageDisplay=866%2C379%2C96" +
                            "&geometryType=esriGeometryPoint&sr=3346&layers=0%3A0";


            return GET(url);
        }

        @Override
        protected void onPostExecute(String result) {
            Log.i("info", result);
            try {
                JSONObject jsonObject = new JSONObject(result);
                int length = jsonObject.length();
                if (length == 0) {
                    everythingIsOk();
                    return;
                }

                JSONArray results = jsonObject.getJSONArray("results");
                if (results.length() == 0) {
                    everythingIsOk();
                    return;
                }

                showWarningMessage();
            } catch (JSONException e) {
                Log.e("error", "error", e);
            }


            stopSelf(serviceStartId);
        }

    }

    private void everythingIsOk() {
        Bundle bundle = new Bundle();
        resultReceiver.send(200, bundle);
    }

    private void showWarningMessage() {
        Bundle bundle = new Bundle();
        resultReceiver.send(100, bundle);
        if (notificationTracker.shouldNotify()) {
            playNotificationSound();
            notificationTracker.updateLastNotificationSound();
        }
    }

    private void playNotificationSound() {
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
            Log.e("error", "error", e);
        }
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return POST(urls[0], null);
        }

        @Override
        protected void onPostExecute(String result) {
        }

    }

}