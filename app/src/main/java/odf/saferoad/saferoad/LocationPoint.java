package odf.saferoad.saferoad;

import android.location.Location;
import android.util.Log;

import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.SpatialReference;

public class LocationPoint {
    protected static final double AXIS_E2 = 2.7222268747421872E11d;
    protected static final double CENTRAL_MERIDAN_SCALE = 0.9998d;
    public static final int COORDINATE_TYPE_LKS = 2;
    public static final int COORDINATE_TYPE_WGS = 1;
    protected static final double DEFAULT_LAT = 54.687554d;
    protected static final double DEFAULT_LNG = 25.280612d;
    protected static final double ORIGIN_LATITUDE = 0.0d;
    protected static final double ORIGIN_LONGITUDE = 24.0d;
    protected static final String[] PROJ4_EPSG_3346;
    protected static final double SCALED_SEMI_MAJOR_AXIS = 6376861.3726d;
    protected static final double SCALED_SEMI_MINOR_AXIS = 6355480.9635372d;
    protected static final double SEMI_MAJOR_AXIS = 6378137.0d;
    protected static final double SEMI_MINOR_AXIS = 6356752.314d;
    protected static final String TAG;
    protected static final double TRUE_ORIGIN_EASTING = 500000.0d;
    protected static final double TRUE_ORIGIN_NORTHING = 0.0d;
    protected float accuracy;
    protected double lat;
    protected double lng;
    protected long time;
    protected String title;
    protected int type;

    static {
        TAG = LocationPoint.class.getSimpleName();
        PROJ4_EPSG_3346 = new String[]{"+proj=tmerc", "+lat_0=0", "+lon_0=24", "+k=0.9998", "+x_0=500000", "+y_0=0", "+ellps=GRS80", "+towgs84=0,0,0,0,0,0,0", "+units=m", "+no_defs"};
    }

    public LocationPoint() {
        this.accuracy = 0.0f;
        this.time = 0;
        this.title = "";
        this.lat = 54.687554d;
        this.lng = 25.280612d;
        this.type = 1;
        this.time = 0;
    }

    public LocationPoint(Location location) {
        this.accuracy = 0.0f;
        this.time = 0;
        this.title = "";
        this.lat = location.getLatitude();
        this.lng = location.getLongitude();
        this.accuracy = location.getAccuracy();
        this.type = 1;
        this.time = System.currentTimeMillis();
    }


    public LocationPoint(double lat, double lng, int type) {
        this.accuracy = 0.0f;
        this.time = 0;
        this.title = "";
        this.lat = lat;
        this.lng = lng;
        this.type = type;
        this.time = System.currentTimeMillis();
    }

    public LocationPoint(double lat, double lng, String title, int type) {
        this.accuracy = 0.0f;
        this.time = 0;
        this.title = "";
        this.lat = lat;
        this.lng = lng;
        this.type = type;
        this.title = title;
        this.time = System.currentTimeMillis();
    }

    public double getLatitude() {
        return this.lat;
    }

    public double getLongitude() {
        return this.lng;
    }

    public float getAccuracy() {
        return this.accuracy;
    }

    public long getTime() {
        return this.time;
    }

    public String getTitle() {
        return this.title;
    }

    public LocationPoint toLKS() {
        if (this.type == 2) {
            return this;
        }
        Point lksPoint = (Point) GeometryEngine.project(new Point(this.lng, this.lat), SpatialReference.create(4326), SpatialReference.create(3346));
        return lksPoint.isEmpty() ? null : new LocationPoint(lksPoint.getY(), lksPoint.getX(), 2);
    }

    public LocationPoint toWGS() {
        if (this.type == 1) {
            return this;
        }
        double g5 = (Math.pow(SCALED_SEMI_MAJOR_AXIS, 2.0d) - Math.pow(SCALED_SEMI_MINOR_AXIS, 2.0d)) / Math.pow(SCALED_SEMI_MAJOR_AXIS, 2.0d);
        double g10 = this.lng - 500000.0d;
        double g19 = ((this.lat - 0.0d) / 6376861.3726d) + Math.toRadians(ORIGIN_LATITUDE);
        double g21 = (((this.lat - 0.0d) - (6355480.9635372d * (((((((1.0d + 0.0016792204056686154d) + (1.0d * Math.pow(0.0016792204056686154d, 2.0d))) + (1.0d * Math.pow(0.0016792204056686154d, 3.0d))) * (g19 - Math.toRadians(ORIGIN_LATITUDE))) - (((((3.0d * 0.0016792204056686154d) + (3.0d * Math.pow(0.0016792204056686154d, 2.0d))) + (2.0d * Math.pow(0.0016792204056686154d, 3.0d))) * Math.sin(g19 - Math.toRadians(ORIGIN_LATITUDE))) * Math.cos(Math.toRadians(ORIGIN_LATITUDE) + g19))) + ((((1.0d * Math.pow(0.0016792204056686154d, 2.0d)) + (1.0d * Math.pow(0.0016792204056686154d, 3.0d))) * Math.sin(2.0d * (g19 - Math.toRadians(ORIGIN_LATITUDE)))) * Math.cos(2.0d * (Math.toRadians(ORIGIN_LATITUDE) + g19)))) - (((1.0d * Math.pow(0.0016792204056686154d, 3.0d)) * Math.sin(3.0d * (g19 - Math.toRadians(ORIGIN_LATITUDE)))) * Math.cos(3.0d * (Math.toRadians(ORIGIN_LATITUDE) + g19)))))) / 6376861.3726d) + g19;
        double g25 = (((this.lat - 0.0d) - (6355480.9635372d * (((((((1.0d + 0.0016792204056686154d) + (1.0d * Math.pow(0.0016792204056686154d, 2.0d))) + (1.0d * Math.pow(0.0016792204056686154d, 3.0d))) * (g21 - Math.toRadians(ORIGIN_LATITUDE))) - (((((3.0d * 0.0016792204056686154d) + (3.0d * Math.pow(0.0016792204056686154d, 2.0d))) + (2.0d * Math.pow(0.0016792204056686154d, 3.0d))) * Math.sin(g21 - Math.toRadians(ORIGIN_LATITUDE))) * Math.cos(Math.toRadians(ORIGIN_LATITUDE) + g21))) + ((((1.0d * Math.pow(0.0016792204056686154d, 2.0d)) + (1.0d * Math.pow(0.0016792204056686154d, 3.0d))) * Math.sin(2.0d * (g21 - Math.toRadians(ORIGIN_LATITUDE)))) * Math.cos(2.0d * (Math.toRadians(ORIGIN_LATITUDE) + g21)))) - (((1.0d * Math.pow(0.0016792204056686154d, 3.0d)) * Math.sin(3.0d * (g21 - Math.toRadians(ORIGIN_LATITUDE)))) * Math.cos(3.0d * (Math.toRadians(ORIGIN_LATITUDE) + g21)))))) / 6376861.3726d) + g21;
        double g6 = 6376861.3726d / Math.pow(1.0d - (Math.pow(Math.sin(g25), 2.0d) * g5), 0.5d);
        double g7 = ((1.0d - g5) * g6) / (1.0d - (Math.pow(Math.sin(g25), 2.0d) * g5));
        double g8 = (g6 / g7) - 1.0d;
        double h2 = ((g25 - (Math.pow(g10, 2.0d) * (Math.tan(g25) / ((2.0d * g6) * g7)))) + (Math.pow(g10, 4.0d) * ((Math.tan(g25) / ((24.0d * g7) * Math.pow(g6, 3.0d))) * (((5.0d + (3.0d * Math.pow(Math.tan(g25), 2.0d))) + g8) - ((9.0d * Math.pow(Math.tan(g25), 2.0d)) * g8))))) - (Math.pow(g10, 6.0d) * ((Math.tan(g25) / ((720.0d * g7) * Math.pow(g6, 5.0d))) * ((61.0d + (90.0d * Math.pow(Math.tan(g25), 2.0d))) + (45.0d * Math.pow(Math.tan(g25), 4.0d)))));
        double h3 = (((Math.toRadians(ORIGIN_LONGITUDE) + (g10 * (Math.pow(Math.cos(g25), -1.0d) / g6))) - (Math.pow(g10, 3.0d) * ((Math.pow(Math.cos(g25), -1.0d) / (6.0d * Math.pow(g6, 3.0d))) * ((g6 / g7) + (2.0d * Math.pow(Math.tan(g25), 2.0d)))))) + (Math.pow(g10, 5.0d) * ((Math.pow(Math.cos(g25), -1.0d) / (120.0d * Math.pow(g6, 5.0d))) * ((5.0d + (28.0d * Math.pow(Math.tan(g25), 2.0d))) + (24.0d * Math.pow(Math.tan(g25), 4.0d)))))) - (Math.pow(g10, 7.0d) * ((Math.pow(Math.cos(g25), -1.0d) / (5040.0d * Math.pow(g6, 7.0d))) * (((61.0d + (662.0d * Math.pow(Math.tan(g25), 2.0d))) + (1320.0d * Math.pow(Math.tan(g25), 4.0d))) + (720.0d * Math.pow(Math.tan(g25), 6.0d)))));
        Log.d(TAG, String.valueOf(Math.toDegrees(h2)));
        Log.d(TAG, String.valueOf(Math.toDegrees(h3)));
        return new LocationPoint(h2, h3, 1);
    }
}
