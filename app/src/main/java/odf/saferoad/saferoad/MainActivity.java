package odf.saferoad.saferoad;

import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;


public class MainActivity extends FragmentActivity {

    private TextToSpeech ttobj;
    private MyResultReceiver resultReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ttobj = new TextToSpeech(getApplicationContext(),
                new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        if (status != TextToSpeech.ERROR) {
                            ttobj.setLanguage(Locale.US);
                            ttobj.setPitch(1.0f);
                            ttobj.setSpeechRate(1.0f);
                        }
                    }
                });
        Intent intent = new Intent(this, FetchAddressIntentService.class);

        resultReceiver = new MyResultReceiver(null);
        intent.putExtra("receiver", resultReceiver);
        startService(intent);

    }

    @Override
    public void onDestroy() {
        if (ttobj != null) {
            ttobj.stop();
            ttobj.shutdown();
        }
        super.onDestroy();
    }

    public void speakText(String toSpeak) {
        Toast.makeText(getApplicationContext(), toSpeak,
                Toast.LENGTH_SHORT).show();
        ttobj.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);

    }

    @Override
    protected void onResume() {
        super.onResume();
//        playNotificationSound();
//        speakText("Good to go");
    }

    private void playNotificationSound() {
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
            Log.e("error", "error", e);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    class UpdateUIWarning implements Runnable {
        public void run() {
            View warningView = findViewById(R.id.view_notification_image_warning);
            View normalImageView = findViewById(R.id.view_notification_image);
            TextView messageView = (TextView) findViewById(R.id.view_notification_text);
            messageView.setText(getResources().getString(R.string.careful));

            warningView.setVisibility(View.VISIBLE);
            normalImageView.setVisibility(View.GONE);
        }
    }

    class UpdateUIPlain implements Runnable {
        public void run() {
            View warningView = findViewById(R.id.view_notification_image_warning);
            View normalImageView = findViewById(R.id.view_notification_image);
            TextView messageView = (TextView) findViewById(R.id.view_notification_text);

            messageView.setText(getResources().getString(R.string.gogogo));
            warningView.setVisibility(View.GONE);
            normalImageView.setVisibility(View.VISIBLE);
        }
    }

    class MyResultReceiver extends ResultReceiver {
        public MyResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            if (resultCode == 100) {
                runOnUiThread(new UpdateUIWarning());
            } else {
                runOnUiThread(new UpdateUIPlain());
            }
        }
    }
}
