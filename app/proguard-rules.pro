-ignorewarnings
-dontoptimize
-dontobfuscate
-dontskipnonpubliclibraryclasses

-ignorewarnings

-renamesourcefileattribute SourceFile

-keepattributes SourceFile,LineNumberTable,*Annotation*

-keepclassmembers enum * { public static **[] values(); public static ** valueOf(java.lang.String); }

-keep class com.feedpresso.mobile.**
-keepclassmembers class odf.saferoad.** { public <init>(...); }
-keepclassmembers class odf.saferoad.** {*; }
-keepclassmembers class odf.saferoad.** {
    *** set*(***);
    *** get*();
}